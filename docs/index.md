### Types of editions
 Value | Description
:-----:|:-----------:
 AC    | Anniversary Collection
 AR    | Arcade Edition
 CE    | Complete Edition
 DE    | Definitive Edition
 DO    | Day One Edition
 DX    | Delux Edition
 EC    | Eternal Collection
 EE    | Enhanced Edition
 GE    | Gold Edition
 GOTY  | Game Of The Year Edition
 HDC   | HD Collection
 LAE   | Launch Edition
 LE    | Legendary Edition
 LC    | Legendary Collection
 LME   | Limited Edition
 SE    | Special Edition
 UE    | Ultimate Edition
