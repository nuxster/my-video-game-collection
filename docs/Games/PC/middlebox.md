### Q
 Status | The names of the games |Publisher    | Condition | Serial number | Covers
:------:|:-----------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Quake II               | Limited Run | Sealed    | PC-QK2SE      | ![cover](./covers/middlebox/PC-QK2SE.jpg)