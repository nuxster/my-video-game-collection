### S
 Status | The names of the games         |Publisher  | Condition | Serial number | Covers
:------:|:-------------------------------|:---------:|:---------:|:-------------:|:--------------:
 [+]    | S.T.A.L.K.E.R.: Тень Чернобыля | GSC       | Sealed    | NONE          | ![cover](./covers/bigbox/STALKER_SHADOW_OF_CHERNOBYL.jpg)
 [+]    | S.T.A.L.K.E.R.: Чистое небо    | GSC       | Sealed    | NONE          | ![cover](./covers/bigbox/STALKER_CLEAR_SKY.jpg)
