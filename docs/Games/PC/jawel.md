### A
 Status | The names of the games                                 | Publisher | Condition | Covers
:------:|:-------------------------------------------------------|:---------:|:---------:|:--------------:
 [+]    | AD&D Baldurs Gate                                      | Акелла    | Sealed    | ![cover](./covers/jawel/624fd92ac0fdc93385e0b17805025c.jpg)
 [+]    | AD&D Baldurs Gate 2 + Shadows of Amn + Throne of Bhaal | Акелла    | Sealed    | ![cover](./covers/jawel/b9537360f0f7b8b736af0ca01171de.jpg)
 [+]    | AD&D Baldurs Gate 2: Shadows of Amn                    | Акелла    | Sealed    | ![cover](./covers/jawel/9b8b7f790f66926b4ec43456996b88.jpg)
 [+]    | AD&D Demon Stone                                       | Акелла    | Sealed    | ![cover](./covers/jawel/06603fccfb81248b889c72d57dc8b4.jpg)
 [+]    | AD&D Dragonshard                                       | Акелла    | Sealed    | ![cover](./covers/jawel/27a76df6f38dea9fcad895c53c5cba.jpg)
 [+]    | AD&D Icewind Dale                                      | 1С        | Sealed    | ![cover](./covers/jawel/c15f9187cc96898e3641d2f363f926.jpg)
 [+]    | AD&D Icewind Dale                                      | Акелла    | Sealed    | ![cover](./covers/jawel/82b622211a6b7b7fcfcbc2f0e36d41.jpg)
 [+]    | AD&D Icewind Dale + Heart of Winter                    | Акелла    | Sealed    | ![cover](./covers/jawel/e5ba72c231fc4de2fe0a6161826f4b.jpg)
 [+]    | AD&D Icewind Dale: Heart of Winter                     | 1С        | Sealed    | ![cover](./covers/jawel/c0c020abccb9dc17f6005de9149c58.jpg)
 [+]    | AD&D Icewind Dale 2                                    | Акелла    | Sealed    | ![cover](./covers/jawel/dd3f77cc65d9509f70a6fe90fd8339.jpg)
 [+]    | AD&D Icewind Dale Gold                                 | Акелла    | Sealed    | ![cover](./covers/jawel/085357cd2454967d62214ae9f60735.jpg)
 [+]    | AD&D Newerwinter Nights                                | 1С        | Sealed    | ![cover](./covers/jawel/6a3edcf775872848d7b679b86d8675.jpg)
 [+]    | AD&D Newerwinter Nights Hordes of the Underdark        | 1С        | Sealed    | ![cover](./covers/jawel/97977d9ec0af06b3957a8c928ad9b3.jpg)
 [+]    | AD&D Newerwinter Nights Shadows of Undertide           | 1С        | Sealed    | ![cover](./covers/jawel/13e6bb148655a91ffb60dd0d71a556.jpg)
 [+]    | AD&D Newerwinter Nights 2                              | Акелла    | Sealed    | ![cover](./covers/jawel/e63c05e8eaf0be04614cbe8c9821bc.jpg)
 [+]    | AD&D Newerwinter Nights 2: Mask of the Betrayer        | Акелла    |           | ![cover](./covers/jawel/b1e9c581f527a17a43c2660b95093f.jpg)
 [+]    | AD&D Newerwinter Nights 2: Mysteries of Westgate       | Акелла    |           | ![cover](./covers/jawel/99840e694aa6efba95edeb15b7e7a5.jpg)
 [+]    | AD&D Newerwinter Nights 2: Storm of Zehir              | Акелла    | Sealed    | ![cover](./covers/jawel/340ce5687daa7a71ff2c960a82cd7d.jpg)
 [+]    | AD&D Pool of Radiance                                  | 1С        | Sealed    | ![cover](./covers/jawel/c1e95f8f35f513d1f69415b0f3fa42.jpg)
 [+]    | AD&D The Temple of Elemental Evil                      | Акелла    | Sealed    | ![cover](./covers/jawel/6f0683f3bf76d5a1b0ea7fb34bae91.jpg)
 [ ]    | Arcanum                                                |           |           |

### B
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | Black & White 2        | EA хит-парад |           | ![cover](./covers/jawel/bccb2b565b21f8099f28d5ffe6f08e.jpg)

### D
 Status | The names of the games              | Publisher    | Condition | Covers
:------:|:------------------------------------|:------------:|:---------:|:--------------:
 [+]    | Dungeon Siege                       | Новый диск   | Sealed    | ![cover](./covers/jawel/ab4ce239789a693dcdbcf9ef54ed05.jpg)
 [+]    | Dungeon Siege: Легенды Араны        | Новый диск   | Sealed    | ![cover](./covers/jawel/c20978cfeb1eafbc7348db634a33e4.jpg)
 [+]    | Dungeon Siege 2                     | 1С           | Sealed    | ![cover](./covers/jawel/c665f475daa3e5ccd03d6562dd9bb6.jpg)
 [ ]    | Dungeon Siege 2: Broken World       |              |           |
 [+]    | Dungeon Siege 3                     |              | Sealed    | ![cover](./covers/jawel/6c10ffe381df1134fab765a83f09d0.jpg)
 [+]    | Disciples: World                    | Акелла       | Sealed    | ![cover](./covers/jawel/1415567bb6f1fb5b2f39235c9fcdbc.jpg)
 [+]    | Disciples: Sacred Lands             | Акелла       | Sealed    | ![cover](./covers/jawel/705eb04ca6fc22ae572736deb1bc01.jpg)
 [+]    | Disciples 2 Gold                    | Акелла       | Sealed    | ![cover](./covers/jawel/52aef5967fddebc1874a3b68362840.jpg)
 [ ]    | Disciples 2: Dark Prophecy          |              |           |
 [ ]    | Disciples 2: Servants of the Dark   |              |           |
 [ ]    | Disciples 2: Guardians of the Light |              |           |
 [+]    | Disciples 2: Rise of the Elves Gold | Акелла       | Sealed    | ![cover](./covers/jawel/c04d2eee674828b877b19a7b9bcd25.jpg)
 [+]    | Disciples 2: Восстание Эльфов       | Акелла       |           | ![cover](./covers/jawel/56ed3bdb500749aef58226ed56af32.jpg)
 [ ]    | Disciples 3: Renaissance            |              |           |
 [+]    | Disciples 3: Resurrection           | Акелла       | Sealed    | ![cover](./covers/jawel/39f6e588f07cb802e3ff1d18162fdb.jpg)
 [ ]    | Disciples 3: Reincarnation          |              |           |
 [+]    | Doom 3                              | 1С           |           | ![cover](./covers/jawel/d2b793bd934c591b079d49ed1882b3.jpg)
 [+]    | Doom 3: Resurrection of Evil        | 1С           |           | ![cover](./covers/jawel/23b2cf5f9b691d8da896880bc589a4.jpg)
 [+]    | Diablo 2 + Lord of Destruction      | Blizzard     | Sealed    | ![cover](./covers/jawel/44ba80ae56b1c580522291216619a1.jpg)
 [ ]    | Diablo 3                            |              |           |

### F
 Status | The names of the games  | Publisher    | Condition | Covers
:------:|:------------------------|:------------:|:---------:|:--------------:
 [+]    | FarCray 3: Blood Dragon | Бука         | Sealed    | ![cover](./covers/jawel/9b001566de323cd8ef53fdfe01eca1.jpg)

### G
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | Gears of War           | 1C           | Sealed    | ![cover](./covers/jawel/14AhmyyTtfM2nOz5U2QToYRoK9Pls0.jpg)

### H
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | Heavy Metal FAKK2      | Бука         | Sealed    | ![cover](./covers/jawel/83f2a43e16cc290553d5a171e9c9f3.jpg)

### M
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | Max Payne              | 1C           | Sealed    | ![cover](./covers/jawel/ca44a7d13c3e13da8974bc9d568127.jpg)
 [ ]    | Max Payne 2            |              |           |

### N
 Status | The names of the games        | Publisher    | Condition | Covers
:------:|:------------------------------|:------------:|:---------:|:--------------:
 [+]    | Need for Speed: Underground   |              |           |
 [+]    | Need for Speed: Underground 2 |              |           |
 [+]    | Need for Speed: Most Wanted   | EA хит-парад | Sealed    | ![cover](./covers/jawel/7b11d3c323459f6bd1d0ed265ae079.jpg)
 [ ]    | Nox                           |              |           |

### P
 Status | The names of the games      | Publisher    | Condition | Covers
:------:|:----------------------------|:------------:|:---------:|:--------------:
 [+]    | Painkiller: Крещёный кровью | Акелла       | Sealed    | ![cover](./covers/jawel/b60b88eb35613f6489ab51417ba86f.jpg)
 [ ]    | Planscape: Torment          |              |           |
 [+]    | Postal 2: Штопор ЖЖОТ!      | Акелла       | Sealed    | ![cover](./covers/jawel/e78447c008dd188af3845d6b69c099.jpg)
 [+]    | Prey                        | 1С           | Sealed    | ![cover](./covers/jawel/39f98b529a0220596ab534ff92cce4.jpg)

### Q
 Status | The names of the games            | Publisher    | Condition | Covers
:------:|:----------------------------------|:------------:|:---------:|:--------------:
 [+]    | Quake 4                           | 1C           | Sealed    | ![cover](./covers/jawel/578d87a0bc4ee58d04d22c93aee318.jpg)
 [+]    | Quake Enemy Territory: Quake Wars | 1C           | Sealed    | ![cover](./covers/jawel/deb56ea092b5d8fd0e969226d274ce.jpg)

### S
 Status | The names of the games             | Publisher    | Condition | Covers
:------:|:-----------------------------------|:------------:|:---------:|:--------------:
 [+]    | Sin                                | Бука         | Sealed    | ![cover](./covers/jawel/3d32f8b3f82c5b54df941ccb817885.jpg)
 [+]    | Supreme Commander                  | Бука         | Sealed    | ![cover](./covers/jawel/3b71fc103d2b45d2c5a3860c0fe252.jpg)
 [+]    | Supreme Commander (blue jawel)     | Бука         | Sealed    | ![cover](./covers/jawel/3b71fc103d2b45d2c5a3860c0fe252_b.jpg)
 [+]    | Supreme Commander: Forget Alliance | Бука         | Sealed    | ![cover](./covers/jawel/a7f9c2490ad06508968c8705dccabf.jpg)
 [+]    | Supreme Commander 2                |              | Sealed    | ![cover](./covers/jawel/87d3e034ff569da72e98d7d8cf8f89.jpg)

### T
 Status | The names of the games                            | Publisher    | Condition | Covers
:------:|:--------------------------------------------------|:------------:|:---------:|:--------------:
 [+]    | The Elder Scrolls 3 Morrowind                     | 1С           |           | ![cover](./covers/jawel/5ae3a048e886baf77c6c78477e30d6.jpg)
 [+]    | The Elder Scrolls 3 Morrowind: Tribunal           | 1С           | Sealed    | ![cover](./covers/jawel/2ecfdd8064b45c16f8941274caa757.jpg)
 [+]    | The Elder Scrolls 3 Morrowind: BloodMoon          | 1С           | Sealed    | ![cover](./covers/jawel/391352fac6d54d06dde5f605cd4688.jpg)
 [+]    | The Elder Scrolls 4 Oblivion                      | 1С           | Sealed    | ![cover](./covers/jawel/f3f3e22acdece6df45b8df44ae38c3.jpg)
 [+]    | The Elder Scrolls 4 Oblivion: Shivering Isles     | 1С           | Sealed    | ![cover](./covers/jawel/6f7a5f8c2c8c08801c14e4957bf7d6.jpg)
 [+]    | The Elder Scrolls 4 Oblivion: Knights of the Nine | 1С           | Sealed    | ![cover](./covers/jawel/a4a946caa533c43dc59e758210cabe.jpg)
 [+]    | The Elder Scrolls 5 Skyrim                        | 1С           | Sealed    | ![cover](./covers/jawel/9819d532377738c1829e30b52296bc.jpg)
 [+]    | Two Worlds GOTY                                   | Акелла       | Sealed    | ![cover](./covers/jawel/bb277f8d5bdbbef0f5b01119a7a9a6.jpg)
 [+]    | Two Worlds 2                                      | Акелла       | Sealed    | ![cover](./covers/jawel/e3d512d1c0428b1bece54b64286b0b.jpg)

### U
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | UFO Extraterrestrials  | Новый диск   |           | ![cover](./covers/jawel/5b0d053b87ccaccb7b28fd6cd1237b.jpg)
 [+]    | Unreal Anthology       | Новый диск   | Sealed    | ![cover](./covers/jawel/6d4ab417e4d8a0c3d4ca2e58dd4fe8.jpg)

### W
 Status | The names of the games                 | Publisher    | Condition | Covers
:------:|:---------------------------------------|:------------:|:---------:|:--------------:
 [+]    | WarCraft 3: Reign of Chaos             |              | Sealed    | ![cover](./covers/jawel/506989fe5a2e3ab15c327b98315be0.jpg)
 [+]    | WarCraft 3: Reign of Chaos             |              | Sealed    | ![cover](./covers/jawel/c546a1f2c4f0d27cfbb03557b1edb6.jpg)
 [+]    | WarCraft 3: The Frozen Throne          |              | Sealed    | ![cover](./covers/jawel/705f12cfcdb76de5996c324a4e7517.jpg)
 [+]    | World of WarCraft: The Burning Crusade |              |           | 
 [+]    | World of WarCraft: Wrath of Lich King  |              | Sealed    | ![cover](./covers/jawel/c5407994d7edab885839af0fd95257.jpg)
 [+]    | World of WarCraft: Cataclysm           |              | Sealed    | ![cover](./covers/jawel/cb0974652266a2074eb31d95155373.jpg)
 [+]    | World of WarCraft: Mists of Pandaria   |              | Sealed    | ![cover](./covers/jawel/5bb7552037ba23c2190ea638e82335.jpg)
 [+]    | World of WarCraft: Warlords of Draenor |              | Sealed    | ![cover](./covers/jawel/ecef783f24c72253ea7b7c53b4c0ba.jpg)
 [ ]    | World of WarCraft: Legion              |              |           |
 [ ]    | World of WarCraft: Battle for Azeroth  |              |           |
 [ ]    | World of WarCraft: Shadowlands         |              |           |
 [ ]    | World of WarCraft: Dragonflight        |              |           |

### Рус
 Status | The names of the games | Publisher    | Condition | Covers
:------:|:-----------------------|:------------:|:---------:|:--------------:
 [+]    | Аллоды Онлайн          | ND Games     | Sealed    | ![cover](./covers/jawel/25ea45787ff89e66a2f207bd672b36.jpg)
 [+]    | Не время для драконов  | 1C           | Sealed    | ![cover](./covers/jawel/1853d4c35b7773773ed9099576abd7.jpg)
 [ ]    | Они                    |              |           |
 [+]    | Ядерный Титбит         | Бука         | Sealed    | ![cover](./covers/jawel/44e749768126955aef008dc8acf3ff.jpg)
