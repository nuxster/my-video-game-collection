### C
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Command & Conquer 3                    | EA          |           |               |

### D
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Diablo 2                               | Blizzard    | Sealed    |               |
 [+]    | Diablo 2: Lord of Destruction          | Blizzard    | Sealed    |               |

### P
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Plane Scape Torment                    | Interplay   | Sealed    |               |

### R
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Red Alert 3                            | EA          |           |               |

### S
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | Star Craft                             | Blizzard    |           |               |
 [+]    | Star Craft 2: Wings of Liberty         | Blizzard    | Sealed    |               |
 [+]    | S.T.A.L.K.E.R.: Зов Припяти            | GSC         | Sealed    |               |

### U
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | UFO Трилогия                           | 1C          |           |               |

### W
 Status | The names of the games                 | Publisher   | Condition | Serial number | Covers
:------:|:---------------------------------------|:-----------:|:---------:|:-------------:|:--------------:
 [+]    | World Of WarCraft                      | Blizzard    | Sealed    |               |
 [+]    | World Of Warcraft: The Burning Crusade | Blizzard    | Sealed    |               |
