### B 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Black                       |        |           |               |               

### D 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Doom 3                      |        |           |               |               

### F 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Far Cry: Instincts          |        |           |               |               

### H 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Half-Life 2                 |        |           |               |               
 [ ]    | Halo: Combat Evolved        |        |           |               |
 [+]    | Halo 2                      | NTSC   | Sealed    | M41 00028     | ![cover](./covers/XBox_Original/M41_00028.jpg)
 [+]    | Halo 2 Multiplayer Map Pack | NTSC   | Sealed    | M41 00075     | ![cover](./covers/XBox_Original/M41_00075.jpg)

### M 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Max Payne                   |        |           |               |
 [ ]    | Max Payne 2                 |        |           |               |

### N 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Need for Speed Most Wanted  |        |           |               |

### S 
 Status | The names of the games      | Region | Condition | Serial number | Covers
:------:|:----------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Syberia                     | NTSC   | Sealed    | NONE          | ![cover](./covers/XBox_Original/SYBERIA_1.jpg)
 [ ]    | Syberia 2                   |        |           |               |

### W
 Status | The names of the games       | Region | Condition | Serial number | Covers
:------:|:-----------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Wolfenstein Return to Castle |        |           |               |