### A
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | AD&D Dark Alliance                         |        | Sealed    | 1062656       | ![cover](./covers/XBox_One_Series/1062656.jpg)
 [ ]    | AD&D Baldur's Gate 1,2: EE                 |        |           |               |
 [ ]    | AD&D Icewind Dale & Planescape Torment: EE |        |           |               |
 [+]    | AD&D Nverwinter Nights: EE                 |        | Sealed    | SKY0007       | ![cover](./covers/XBox_One_Series/SKY0007.jpg)
 [ ]    | AD&D Pillars of Eternity: CE               |        |           |               |
 [ ]    | AD&D Pillars of Eternity 2 Deadfire: UE    |        |           |               |

### C
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [x]    | Cyberpunk 2077                             |        | Sealed    | 920 06155     | ![cover](./covers/XBox_One_Series/920_06155.jpg)
 [x]    | Cyberpunk 2077: UE                         |        | Sealed    | 676 41696     | ![cover](./covers/XBox_One_Series/676_41696.jpg)
 [ ]    | Cygni: All Guns Blazing                    |        |           |               |
 [+]    | Crysis Remastered Trilogy                  |        | Sealed    | NONE          | ![cover](./covers/XBox_One_Series/CRYSIS_REMASTERED_TRILOGY.jpg)
 
### D
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Darksiders: Warmastered Edition            |        |           |               |
 [ ]    | Darksiders II: Deathinitive Edition        |        |           |               |
 [+]    | Darksiders III                             |        | Sealed    | 800 70814     | ![cover](./covers/XBox_One_Series/800_70814.jpg)
 [+]    | Darksiders: Genesis                        |        | Sealed    | 800 74423     | ![cover](./covers/XBox_One_Series/800_74423.jpg)
 [+]    | Dead Space                                 |        | Sealed    | NONE          | ![cover](./covers/XBox_One_Series/40a69602727efa2faff0694f6735e5e2.jpg)
 [+]    | Doom 3: BFG Edition                        |        | Sealed    | ZB17104BCWR2  | ![cover](./covers/XBox_One_Series/ZB17104BCWR2.jpg)
 [ ]    | Diablo IV                                  |        |           |               |
 [+]    | Diablo 3: Reaper Of Souls                  |        | Sealed    | 87184 206RU   | ![cover](./covers/XBox_One_Series/87184_206RU.jpg)
 
### G
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Gears of War: UE                           |        | Sealed    | V5 00010      | ![cover](./covers/XBox_One_Series/4V5_00010.jpg)
 [ ]    | Gears of War 4: UE                         |        |           |               |
 [+]    | Gears of War 4                             |        | Sealed    | 4V9 00013     | ![cover](./covers/XBox_One_Series/4V9_00013.jpg)
 [ ]    | Gears 5: UE                                |        |           |               |
 [+]    | Gears Tactics                              |        | Sealed    | GFT 00015     | ![cover](./covers/XBox_One_Series/GFT_00015.jpg)
 
### H
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Halo: The Master Chief Collecction         |        | Sealed    | RQ2-00028     | ![cover](./covers/XBox_One_Series/RQ2-00028.jpg)
 [+]    | Halo Wars 2: UE                            |        | Sealed    | 7GS-00001     | ![cover](./covers/XBox_One_Series/7GS-00001.jpg)
 [+]    | Halo 5                                     |        | Sealed    | U9Z-00055     | ![cover](./covers/XBox_One_Series/U9Z-00055.jpg)
 [+]    | Halo Infinite                              |        | Sealed    | HM7 00020     | ![cover](./covers/XBox_One_Series/HM7_00020.jpg)
 
### I
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Injustice 2                                |        | Sealed    | 922 05023     | ![cover](./covers/XBox_One_Series/922_05023.jpg)
 
### K
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Killer Instinct                            |        | Sealed    | 4W2 00020     | ![cover](./covers/XBox_One_Series/4W2_00020.jpg)
 
### M
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Mass Effect: LE                            |        | Sealed    | 108322901601  | ![cover](./covers/XBox_One_Series/108322901601.jpg)
 [ ]    | Metro REDUX                                |        |           |               |  
 [ ]    | Metro: Exodus                              |        |           |               |   
 [+]    | Middle-Earth: Shadow of War                |        | Sealed    | 922 09410     | ![cover](./covers/XBox_One_Series/922_09410.jpg)
 [ ]    | Middle-Earth: Shadow of Mordor GOTY        |        |           |               |   
 [ ]    | Middle-Earth: Shadow of War DE             |        |           |               |   
 [+]    | Minecraft                                  |        | Sealed    | 44Z-00020     | ![cover](./covers/XBox_One_Series/44Z-00020.jpg)
 [ ]    | Mortal Kombat X                            |        |           |               |
 [ ]    | Mortal Kombat XL                           |        |           |               |
 [ ]    | Mortal Kombat 11 UE                        |        |           |               |
 
### N
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Need for Speed Hot Pursuit Remastered      |        |           |               |
 [ ]    | No Man's Sky                               |        |           |               |

### R
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Recore                                     |        | Sealed    | 9Y4 00017     | ![cover](./covers/XBox_One_Series/9Y4_00017.jpg)
 [+]    | Rebocop: Rogue City                        |        | Sealed    | NONE          | ![cover](./covers/XBox_One_Series/879f9bf718b1bb08e225a7e66735e683.jpg)

### S
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Shadow Tactics                             |        | Sealed    | 583 60200     | ![cover](./covers/XBox_One_Series/583_60200.jpg)
 [+]    | Shovel Knight: Treasure Trove              |        | Sealed    | NONE          | ![cover](./covers/XBox_One_Series/3715aa0bc980cb8ba857c31d6735e22a.jpg)
 [ ]    | Skyrim                                     |        |           |               |
 [ ]    | Star Wars: Squadrons                       |        |           |               |
 [ ]    | Star Wars: JEDI Fallen Order               |        |           |               | 
 [ ]    | Star Wars Battlefront                      |        |           |               | 
 [ ]    | Star Wars: Battlefront 2                   |        |           |               | 
 [+]    | Sunset Overdrive DO                        |        | Sealed    | 3QT 00028     | ![cover](./covers/XBox_One_Series/3QT_00028.jpg)

### T
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Titanfall                                  |        |           | 101281401601  | ![cover](./covers/XBox_One_Series/101281401601.jpg)
 [+]    | The Evil Within                            |        | Sealed    | IL-49021-EUK  | ![cover](./covers/XBox_One_Series/IL-49021-EUK.jpg)
 [+]    | The Evil Within 2                          |        | Sealed    | IL-16494-EUK  | ![cover](./covers/XBox_One_Series/IL-16494-EUK.jpg)

### U
 Status | The names of the games                     | Region | Condition | Serial number | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Unravel Yarny Bundle                       |        | Sealed    | 107593701601  | ![cover](./covers/XBox_One_Series/107503701601.jpg)
