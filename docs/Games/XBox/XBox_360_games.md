### C
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Castlevania: Lord of Shadow   |        |           |               |
 [+]    | Castlevania: Lord of Shadow 2 | PAL    | Sealed    | 270 38970     | ![cover](./covers/XBox_360/270_38970.jpg)

### D
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Disneyland Adventures         | PAL    | Sealed    | KQF 00017     | ![cover](./covers/XBox_360/KQF_00017.jpg)

### F
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | FarCry: Compilation           | NTSC   | Sealed    | 529071-CVRT   | ![cover](./covers/XBox_360/529071-CVRT.jpg)

### G
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Gears of War Judgment         | PAL    | Sealed    | K7L 00018     | ![cover](./covers/XBox_360/K7L_00018.jpg)
 [ ]    | Gears of War 1                |        |           |               |
 [+]    | Gears of War 2                | PAL    | Sealed    | C3U 00082     | ![cover](./covers/XBox_360/C3U_00082.jpg)
 [+]    | Gears of War 3                | PAL    | Sealed    | D9D 00018     | ![cover](./covers/XBox_360/D9D_00018.jpg)

### H
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Halo Reach                    | PAL    | Sealed    | HEA 0057      | ![cover](./covers/XBox_360/HEA_0057.jpg)
 [+]    | Halo Combat Evolved           | PAL    | Sealed    | E6H 00058     | ![cover](./covers/XBox_360/E6H_00058.jpg)
 [+]    | Halo 3                        | PAL    | Sealed    | DF3 00067     | ![cover](./covers/XBox_360/DF3_00067.jpg)
 [+]    | Halo ODST                     | PAL    | Sealed    | 5EA 00024     | ![cover](./covers/XBox_360/5EA_00024.jpg)
 [+]    | Halo 4                        | PAL    | Sealed    | HND 00063     | ![cover](./covers/XBox_360/HND_00063.jpg)
 [+]    | Halo Wars                     | PAL    | Sealed    | S3M 00022     | ![cover](./covers/XBox_360/S3M_00022.jpg)

### K
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Kinect Adventures!            | PAL    | Sealed    | NONE          | ![cover](./covers/XBox_360/KINECT_ADVENTURES!.jpg)

### Q
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [ ]    | Quake 4                       |        |           |               |

### S
 Status | The names of the games        | Region | Condition | Serial number | Covers 
:------:|:------------------------------|:------:|:---------:|:-------------:|:--------------:
 [+]    | Supreme Commander 2           | PAL    | Sealed    | 900 40359     | ![cover](./covers/XBox_360/900_40359.jpg)
