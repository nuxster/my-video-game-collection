### A
 Status | The names of the games                     | Region | Condition | Serial number                 | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [+]    | AD&D Baldur's Gate: EE                     |        | Sealed    | NONE                          | ![cover](./covers/Switch/BG_I_II_EE.jpg)
 [+]    | AD&D Icewind Dale & Planescape Torment: EE |        | Sealed    | HAC-P-ARSGA TSA-HAC-ARSFA-UKV | ![cover](./covers/Switch/HAC-P-ARSGA_TSA-HAC-ARSFA-UKV.jpg)
 [+]    | AD&D Nweverwinter Nights: EE               |        | Sealed    | NONE                          | ![cover](./covers/Switch/NN_EE.jpg)
 [ ]    | AD&D Pillars of Eternity                   |        |           |                               |
 [+]    | Another world / Flashback                  |        | Sealed    | HAC-P-AU8WA HAC-AU8WA-EUR     | ![cover](./covers/Switch/HAC-P-AU8WA_HAC-AU8WA-EUR.jpg)
 [ ]    | Axiom Verge                                |        |           |                               |
 [ ]    | Axiom Verge 2                              |        |           |                               |

### B
 Status | The names of the games                     | Region | Condition | Serial number                 | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | Bayonetta                                  |        |           |                               |               
 [ ]    | Bayonetta 2                                |        |           |                               |
 [ ]    | Bayonetta 3                                |        |           |                               |
 [+]    | Bloodstained: Curse of The Moon Chronicles |        | Sealed    | HAC-P-BCDPA TSA-HAC-BCDPA-JPN | ![cover](./covers/Switch/HAC-P-BCDPA_TSA-HAC-BCDPA-JPN.jpg)
 [+]    | Bloodstained: Ritual of The Night          |        | Sealed    | HAC-P-AB4PA TSA-HAC-AB4PA-UKV | ![cover](./covers/Switch/HAC-P-AB4PA_TSA-HAC-AB4PA-UKV.jpg)
 [ ]    | Borderlands: LC

### C
 Status | The names of the games        | Region | Condition | Serial number                 | Covers 
:------:|:------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | Capcom Belt Action Collection |        |           |                               |
 [+]    | Castlevania: AC               |        | Sealed    | HAC-P-ATA9B LR106CVR          | ![cover](./covers/Switch/HAC-P-ATA9B_LR106CVR.jpg)
 [+]    | Commandos 2: HD Remaster      |        | Sealed    | HAC-P-AXNYA TSA-HAC-AXNYA-UXP | ![cover](./covers/Switch/HAC-P-AXNYA_TSA-HAC-AXNYA-UXP.jpg)
 [ ]    | Contra AC                     |        |           |                               |
 [+]    | Contra: Rogue Corps           |        | Sealed    | HAC-P-ATCFA 27104-CS          |![cover](./covers/Switch/HAC-P-ATCFA_27104-CS.jpg)
 [ ]    | Cuphead                       |        |           |                               |

### D
 Status | The names of the games                    | Region | Condition | Serial number                 | Covers 
:------:|:------------------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | DAEMON X MACHINA                          |        |           |                               |
 [+]    | Darksiders: Genesis                       |        | Sealed    | HAC-P-ATYHA TSA-HAC-ATYHA-EUR | ![cover](./covers/Switch/HAC-P-ATYHA_TSA-HAC-ATYHA-EUR.jpg)
 [+]    | Darksiders: Warmastered Edition	    |        | Sealde    | HAC-P-ASFSA TSA-HAC-ASFSA-EUR | ![cover](./covers/Switch/HAC-P-ASFSA_TSA-HAC-ASFSA-EUR.jpg)
 [+]    | Darksiders II: Deathinitive Edition       |        | Sealed    | HAC-P-ATGPA TSA-HAC-ATGPA-EUR | ![cover](./covers/Switch/HAC-P-ATGPA_TSA-HAC-ATGPA-EUR.jpg)
 [+]    | Darksiders III                            |        | Sealed    | HAC-P-A3C8A TSA-HAC-A3C8A-EUR | ![cover](./covers/Switch/HAC-P-A3C8A_TSA-HAC-A3C8A-EUR.jpg)
 [+]    | Doom 1,2,3 cover-1                        |        | Sealed    | HAC-P-A4KYA LR102CVR          | ![cover](./covers/Switch/HAC-P-A4KYA_LR102CVR.jpg)
 [+]    | Doom 1,2,3 cover-2                        |        | Sealed    | HAC-P-A4KYA LR102CVR          | ![cover](./covers/Switch/HAC-P-A4KYA_LR102CVR_2.jpg)
 [+]    | Doom 1,2,3 BigBox                         |        | Sealed    | HAC-P-A4KYA LR102CVR          | ![cover](./covers/Switch/HAC-P-A4KYA_LR102CVR_3.jpg)
 [+]    | Doom64                                    |        | Sealed    | HAC-P-UMA LR081CVR            | ![cover](./covers/Switch/HAC-P-UMA_LR081CVR.jpg)
 [ ]    | Doom 2016                                 |        |           |                               |
 [ ]    | Doom Eternal                              |        |           |                               |
 [ ]    | Doom 3: BFG Edition                       |        |           |                               |
 [ ]    | Double Dragon Gaiden: Rise of the Dragons |        |           |                               |
 [+]    | Disney Classic Games Collection           |        | Sealed    | HAC-P-AVEPD 3016363           | ![cover](./covers/Switch/HAC-P-AVEPD_3016363.jpg)
 [+]    | Dusk                                      |        | Sealed    | HAC-P-A76WA LR118CVR          | ![cover](./covers/Switch/HAC-P-A76WA_LR118CVR.jpg)
 [+]    | Diablo 3                                  |        | Sealed    | HAC-P-AQ3FA TSA-HAC-AQ3FA-UKV | ![cover](./covers/Switch/HAC-P-AQ3FA_TSA-HAC-AQ3FA-UKV.jpg)

### F
 Status | The names of the games | Region | Condition | Serial number                 | Covers 
:------:|:-----------------------|:------:|:---------:|:-----------------------------:|:-------:
 [+]    | Felix The Cat		 |        | Sealed    | HAC-P-BFESA LRS203            | ![cover](./covers/Switch/HAC-P-BFESA_LRS203.jpg)
 [+]    | Flashback              |        | Sealed    | HAC-P-AFD5A TSA-HAC-AFD5A-EUR | ![cover](./covers/Switch/HAC-P-AFD5A_TSA-HAC-AFD5A-EUR.jpg)
 [ ]    | Flashback 2            |        |           |                               |
 [+]    | Front Mission 1ST: LME |        | Sealed    | HAC-P-A7UQA HAC-A7UQA-EUR     | ![cover](./covers/Switch/HAC-P-A7UQA_HAC-A7UQA-EUR.jpg)
 [ ]    | Front Mission 2        |        |           |				      |

### G
 Status | The names of the games         | Region | Condition | Serial number                 | Covers 
:------:|:-------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [+]    | Gleylancer & Gynoug Combo Pack |        | Sealed    | HAC-P-BEQMA LRS227            | ![cover](./covers/Switch/HAC-P-BEQMA_LRS227.jpg)
 [+]    | Gothic I, II                   |        | Sealed    | HAC-P-BG9HA TSA-HAC-BG9HA-EUR | ![cover](./covers/Switch/HAC-P-BG9HA_TSA-HAC-BG9HA-EUR.jpg)

### I
 Status | The names of the games | Region | Condition | Serial number                 | Covers 
:------:|:-----------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | Into the Breach        |        |           |                               |
 [+]    | Ion Fury               |        |           | HAC-P-ASTTA TSA-HAC-ASTTA-UKV | ![cover](./covers/Switch/HAC-P-ASTTA_TSA-HAC-ASTTA-UKV.jpg)

### J
 Status | The names of the games                 | Region | Condition | Serial number         | Covers 
:------:|:---------------------------------------|:------:|:---------:|:---------------------:|:-------:
 [+]    | Jurassic Park Classic Games Collection |        | Sealed    | HAC-P-BDU6A NSW-JURPK | ![cover](./covers/Switch/HAC-P-BDU6A_NSW-JURPK.jpg)

### L
 Status | The names of the games | Region | Condition | Serial number                 | Covers 
:------:|:-----------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | Little Nightmares 1 CE |        |           |                               |
 [+]    | Little Nightmares 2 DO |        | Sealed    | HAC-P-AU56A TSA-HAC-AU56A-UKV | ![cover](./covers/Switch/HAC-P-AU56A_TSA-HAC-AU56A-UKV.jpg)
 [ ]    | Little Nightmares 3    |        |           |                               | 

### M
 Status | The names of the games                     | Region | Condition | Serial number                 | Covers 
:------:|:-------------------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | Mega Man 11                                |        |           |                               |
 [ ]    | Mega Man Battle Network Legacy Collection  |        |           |                               |
 [ ]    | Mega Man Legacy Collection                 |        |           |                               |
 [ ]    | Mega Man Legacy Collection 2               |        |           |                               |
 [ ]    | Mega Man X Legacy Collection               |        |           |                               |
 [ ]    | Mega Man X Legacy Collection 2             |        |           |                               |
 [ ]    | Mega Man Zero/ZX Legacy Collection         |        |           |                               |
 [+]    | Metroid Dread                              |        | Sealed    | HAC-P-AYL8A TRA-HAC-AYL8A-ITA | ![cover](./covers/Switch/HAC-P-AYL8A_TRA-HAC-AYL8A-ITA.jpg)
 [ ]    | Metroid Prime 4                            |        |           |                	          |
 [+]    | Metroid Prime Remastered                   |        | Sealed    | HAC-P-A3SDA TRA-HAC-A3SDA-UKV | ![cover](./covers/Switch/HAC-P-A3SDA_TRA-HAC-A3SDA-UKV.jpg)
 [ ]    | Metal Gear Solid: Master Collection Vol. 1 |        |           |                               |

### O
 Status | The names of the games | Region | Condition | Serial number  | Covers 
:------:|:-----------------------|:------:|:---------:|:--------------:|:-------:
 [ ]    | Ori: The Collection    |        |           |                |                        

### P
 Status | The names of the games | Region | Condition | Serial number                 | Covers 
:------:|:-----------------------|:------:|:---------:|:-----------------------------:|:-------:
 [+]    | Postal: REDUX          |        | Sealed    | HAC-P-AX4TA LRPOCVR           | ![cover](./covers/Switch/HAC-P-AX4TA_LRPOCVR.jpg)
 [+]    | Prodeus                |        | Sealed    | HAC-P-A7SJA TSA-HAC-A7SJA-UKV | ![cover](./covers/Switch/HAC-P-A7SJA_TSA-HAC-A7SJA-UKV.jpg)

### Q
 Status | The names of the games | Region | Condition | Serial number        | Covers 
:------:|:-----------------------|:------:|:---------:|:--------------------:|:-------:
 [+]    | Quake                  |        | Sealed    | HAC-P-AZPQA LR119CVR | ![cover](./covers/Switch/HAC-P-AZPQA_LR119CVR.jpg)
 [+]    | Quake II               |        | Sealed    | HAC-P-BADFA LRS207   | ![cover](./covers/Switch/HAC-P-BADFA_LRS207.jpg)

### R
 Status | The names of the games              | Region | Condition | Serial number        | Covers 
:------:|:------------------------------------|:------:|:---------:|:--------------------:|:-------:
 [+]    | Rocket Knight Adventures ReSparked! |        | Sealed    | HAC-P-BAXWA LRS209   | ![cover](./covers/Switch/HAC-P-BAXWA_LRS209.jpg)

### S
 Status | The names of the games              | Region | Condition | Serial number                 | Covers 
:------:|:------------------------------------|:------:|:---------:|:-----------------------------:|:-------:
 [ ]    | SEGA Genesis Classics               |        |           |                               |
 [ ]    | SEGA Mega Drive Classics            |        |           |                               |
 [+]    | Showel Knight                       |        | Sealed    | HAC-P-AB7PA TSA-HAC-AB7PA-UKV | ![cover](./covers/Switch/HAC-P-AB7PA_TSA-HAC-AB7PA-UKV.jpg)
 [+]    | Signalis                            |        | Sealed    | HAC-P-A896A TSA-HAC-A896A-UKV | ![cover](./covers/Switch/HAC-P-A896A_TSA-HAC-A896A-UKV.jpg)
 [ ]    | Syberia                             |        |           |                               |
 [ ]    | Syberia 2                           |        |           |                               |
 [ ]    | Syberia 3                           |        |           |                               |
 [ ]    | Star Wars: Racer and Commando Combo |        |           |                               |
 [ ]    | Star Wars: Jedi Knight Collection   |        |           |                               |
 [ ]    | Star Wars: Republic Commando        |        |           |                               |

### T
 Status | The names of the games                                 | Region | Condition | Serial number                  | Covers 
:------:|:-------------------------------------------------------|:------:|:---------:|:------------------------------:|:-------:
 [+]    | The Legend of Zelda: Breath of the Wild                |        | Sealed    | HAC-P-AAAAA TRA-HAC-AAAAA-UKV  | ![cover](./covers/Switch/HAC-P-AAAAA_TRA-HAC-AAAAA-UKV.jpg)
 [+]    | The Legend of Zelda: Echoes of Wisdom                  |        | Sealed    | HAC-P-BDGEA TRA-HAC-BDGEA-UKV  | ![cover](./covers/Switch/HAC-P-BDGEA_TRA-HAC-BDGEA-UKV.jpg)
 [+]    | The Legend of Zelda: Link's Awakening                  |        | Sealed    | HAC-P-AR3NA TRA-HAC-AR3NA-UKV  | ![cover](./covers/Switch/HAC-P-AR3NA_TRA-HAC-AR3NA-UKV.jpg)
 [+]    | The Legend of Zelda: Link's Awakening                  |        | Sealed    | HAC-P-AR3NA TRA-HAC-AR3NA-CHT  | ![cover](./covers/Switch/HAC-P-AR3NA_TRA-HAC-AR3NA-CHT.jpg)
 [+]    | Tetris 99                                              |        | Sealed    | HAC-R-ARZNB TRA-HAC-ARZNB-RURU | ![cover](./covers/Switch/HAC-R-ARZNB_TRA-HAC-ARZNB-RURU.jpg)
 [+]    | The Legend of Zelda: Skyward Sword HD                  |        | Sealed    | HAC-P-AZ89A TRA-HAC-AZ89A-RURU | ![cover](./covers/Switch/HAC-P-AZ89A_TRA-HAC-AZ89A-RURU.jpg)
 [+]    | The Legend of Zelda: Skyward Sword HD                  |        | Sealed    | HAC-P-AZ89A TRA-HAC-AZ89A-CHT  | ![cover](./covers/Switch/HAC-P-AZ89A_TRA-HAC-AZ89A-CHT.jpg)
 [+]    | The Legend of Zelda: Tears of the Kingdom              |        | Sealed    | HAC-P-AXN7A TRA-HAC-AXN7A-UKV  | ![cover](./covers/Switch/HAC-P-AXN7A_TRA-HAC-AXN7A-UKV.jpg)
 [+]    | Teenage Mutant Ninja Turtles: The Cowabunga Collection |        | Sealed    | HAC-P-A42SA TSA-HAC-A42SA-UXP  | ![cover](./covers/Switch/HAC-P-A42SA_TSA-HAC-A42SA-UXP.jpg)
 [+]    | Teenage Mutant Ninja Turtles: Shredder's Revenge       |        | Sealed    | HAC-P-A3UVA TSA-HAC-A3UVA-EUR  | ![cover](./covers/Switch/HAC-P-A3UVA_TSA-HAC-A3UVA-EUR.jpg)
 [+]    | Teenage Mutant Ninja Turtles: Warth of the Mutants     |        | Sealed    | HAC-P-BEV4A TSA-HAC-BEV4A-EUR  | ![cover](./covers/Switch/HAC-P-BEV4A_TSA-HAC-BEV4A-EUR.jpg)
 [+]    | Teenage Mutant Ninja Turtles: Mutants Unleashed        |        | Sealed    | HAC-P-BDQAA TSA-HAC-BDQAA-UXP  | ![cover](./covers/Switch/HAC-P-BDQAA_TSA-HAC-BDQAA-UXP.jpg)
 [+]    | Toaplan Arcade Garage: KYUKYOKU TIGER-HELO             |        | Sealed    | HAC-P-BDL3A LRS217             | ![cover](./covers/Switch/HAC-P-BDL3A_LRS217.jpg)
 [+]    | Tomb Raider I-II-III Remastered		            	 |	      | Sealed    | HAC-P-BDHRA TSA-HAC-BDHRA-UKV  | ![cover](./covers/Switch/HAC-P-BDHRA_TSA-HAC-BDHRA-UKV.jpg)
 [ ]    | Turok: Dinosaur Hunter                                 |        |           |                                | 
 [ ]    | Turok 2: Seeds of Evil                                 |        |           |                                | 
