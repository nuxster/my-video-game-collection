### A
 Status | The names of the games                      | Region | Condition | Serial number | Covers 
:------:|:--------------------------------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | AD&D Baldur's Gate: EE                      |        | Sealed    | CUSA 16052    | ![cover](./covers/PS4/CUSA_16052.jpg)
 [+]    | AD&D Neverwinter Nightis: EE                |        | Sealed    | CUSA 15938    | ![cover](./covers/PS4/CUSA_15938.jpg)
 [+]    | AD&D Planescape: Torment & Icewind Dale: EE |        | Sealed    | CUSA 16053    | ![cover](./covers/PS4/CUSA_16053.jpg)
 [+]    | AD&D Torment: Tides of Numenera: DO         |        | Sealed    | CUSA 05827    | ![cover](./covers/PS4/CUSA_05827.jpg)
 [+]    | Armored Core VI: Fires Of Rubicon: LAE      |        | Sealed    | CUSA 32599    | ![cover](./covers/PS4/CUSA_32599.jpg)
 [+]    | Axiom Verge                                 |        | Sealed    | CUSA 02312    | ![cover](./covers/PS4/CUSA_02312.jpg)
 [+]    | Axiom Verge 2                               |        | Sealed    | 2107692       | ![cover](./covers/PS4/2107692.jpg)
 [+]    | Axiom Verge 1/2                             |        | Sealed    | 2107922       | ![cover](./covers/PS4/2107922.jpg)

### B
 Status | The names of the games                        | Region | Condition | Serial number | Covers 
:------:|:----------------------------------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | Batman: Return to Arkham                      |        |           | CUSA 04607    | ![cover](./covers/PS4/CUSA_04607.jpg)
 [+]    | Batman: Arkham Knight                         |        | Sealed    | CUSA 00135    | ![cover](./covers/PS4/CUSA_00135.jpg)
 [+]    | Bayonetta & Vanquish: 10th Anniversary Bundle |        | Sealed    | CUSA 18495    | ![cover](./covers/PS4/CUSA_18495.jpg)
 [+]    | Bioshock: The Collection                      |        | Sealed    | 2101409       | ![cover](./covers/PS4/2101409.jpg)
 [+]    | Bloodborne                                    |        | Sealed    | CUSA 00207/H  | ![cover](./covers/PS4/CUSA_00207_H.jpg)
 [+]    | Bloodborne                                    |        |           | CUSA 03173    | ![cover](./covers/PS4/CUSA_03173.jpg)

### C
 Status | The names of the games   | Region | Condition | Serial number | Covers 
:------:|:-------------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | Call of Cthulhu          |        |           | CUSA 04850    | ![cover](./covers/PS4/CUSA_04850.jpg)
 [+]    | Castlevania: AC          |        | Sealed    | 2107741       | ![cover](./covers/PS4/2107741.jpg)
 [+]    | Castlevania: Requiem     |        | Sealed    | 2108587       | ![cover](./covers/PS4/2108587.jpg)
 [+]    | Crash Bandicoot: Trilogy |        | Sealed    | CUSA 11870    | ![cover](./covers/PS4/CUSA_11870.jpg)
 [ ]    | Crash Bandicoot 4        |        |           |               | 
 [+]    | Contra: AC               |        | Sealed    | 2108558       | ![cover](./covers/PS4/2108588.jpg) 


### D
 Status | The names of the games   | Region | Condition | Serial number                            | Covers 
:------:|:-------------------------|:------:|:---------:|:----------------------------------------:|:-------:
 [+]    | Dark Souls: Trilogy      |        | Sealed    | CUSA 08495<br> CUSA 01589<br> CUSA 07439 | ![cover](./covers/PS4/CUSA_08495_01589_07439.jpg)				   	    		       
 [+]    | Death Stranding          |        | Sealed    | CUSA 12607/RSC | ![cover](./covers/PS4/CUSA_12607_RSC.jpg)
 [+]    | Devil May Cry:  HDC      |        | Sealed    | CUSA 09263     | ![cover](./covers/PS4/CUSA_09263.jpg)
 [+]    | Detroit: Стать человеком |        | Sealed    | CUSA 08308/RSC | ![cover](./covers/PS4/CUSA_08308_RSC.jpg)
 [+]    | Diablo 3: EE             |        | Sealed    | CUSA 12532     | ![cover](./covers/PS4/CUSA_12532.jpg)
 [+]    | Doom 4                   |        |           | CUSA 02092     | ![cover](./covers/PS4/CUSA_02092.jpg)
 [+]    | Doom Eternal             |        | Sealed    | CUSA 17933     | ![cover](./covers/PS4/CUSA_17933.jpg)
 [+]    | Doom 64                  |        |           | 2107032        | ![cover](./covers/PS4/2107032.jpg)   

### E
 Status | The names of the games     | Region | Condition | Serial number | Covers 
:------:|:---------------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | Elder Scrolls V Skyrim: SE |        | Sealed    | CUSA 05486    | ![cover](./covers/PS4/CUSA_05486.jpg)

### F
 Status | The names of the games | Region | Condition | Serial number | Covers 
:------:|:-----------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | FarCry 3               |        | Sealed    | CUSA 10326    | ![cover](./covers/PS4/CUSA_10326.jpg)
 [+]    | FarCry 4               |        | Sealed    | CUSA 00462    | ![cover](./covers/PS4/CUSA_00462.jpg)
 [+]    | FarCry: Primal         |        | Sealed    | CUSA 03310    | ![cover](./covers/PS4/CUSA_03310.jpg)
 [+]    | FarCry 5               |        | Sealed    | CUSA 05848    | ![cover](./covers/PS4/CUSA_05848.jpg)
 [+]    | FarCry: New Down       |        | Sealed    | CUSA 13886    | ![cover](./covers/PS4/CUSA_13886.jpg)
 [+]    | FarCry 6               |        | Sealed    | CUSA 15779    | ![cover](./covers/PS4/CUSA_157719.jpg)
 [+]    | Fallout 4: GOTY        |        | Sealed    | CUSA 03450    | ![cover](./covers/PS4/CUSA_03450.jpg)

### G
 Status | The names of the games | Region | Condition | Serial number  | Covers 
:------:|:-----------------------|:------:|:---------:|:--------------:|:-------:
 [+]    | God of war 3           |        | Sealed    | CUSA 01715     | ![cover](./covers/PS4/CUSA_01715.jpg)
 [+]    | God of war 4           |        | Sealed    | CUSA 07410/H   | ![cover](./covers/PS4/CUSA_07410_H.jpg)
 [+]    | God of war 4           |        |           | CUSA 07412/RSC | ![cover](./covers/PS4/CUSA_07412_RSC.jpg)
 [+]    | God of war: Ragnarok   |        | Sealed    | CUSA 34390     | ![cover](./covers/PS4/CUSA_34390.jpg)
 [+]    | Grand Turismo: Sport   |        |           | CUSA 02168     | ![cover](./covers/PS4/CUSA_02168.jpg)
 [ ]    | Gravity Rush           |        |           |                |
 [ ]    | Gravity Rush 2         |        |           |                |
 [+]    | GTA 5                  |        |           | CUSA 00411     | ![cover](./covers/PS4/CUSA_00411.jpg)

### H
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Heavy Rain & За гранью: Две души  |        | Sealed    | CUSA 00512_RSC   | ![cover](./covers/PS4/CUSA_00512_RSC.jpg)
 [+]    | Horizon Zero Dawn: CE             |        | Sealed    | CUSA 10213       | ![cover](./covers/PS4/CUSA_10213.jpg)
 [+]    | Horizon Zero Dawn: Forbidden West |        | Sealed    | CUSA 24705       | ![cover](./covers/PS4/CUSA_24705.jpg)
 [+]    | Hotline Miami: Collection         |        | Sealed    | CUSA 00528 00371 | ![cover](./covers/PS4/CUSA_00528_00371.jpg)

### I
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | inFamous: Second Son              |        | Sealed    | CUSA 00004/H     | ![cover](./covers/PS4/CUSA_00004_H.jpg)
 [+]    | Ion Fury                          |        | Sealed    | CUSA 14704       | ![cover](./covers/PS4/CUSA_14704.jpg)

### J
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Jay and Silent Bob: Mall Brawl AE |        | Sealed    | 2107454          | ![cover](./covers/PS4/2107454.jpg)

### K
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Killzone: Shadow Fall             |        | Sealed    | CUSA 00002       | ![cover](./covers/PS4/CUSA_00002.jpg)
 [+]    | Killzone: В плену сумрака         |        |           | CUSA 00002/H/RSC | ![cover](./covers/PS4/CUSA_00002_H_RSC.jpg)

### L
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | LittleBig Plannet 3               |        | Sealed    | CUSA 00063/H/RSC | ![cover](./covers/PS4/CUSA_00063_H_RSC.jpg)

### M
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Mass Effect: Andromeda            |        | Sealed    | CUSA 02491       | ![cover](./covers/PS4/CUSA_02491.jpg)
 [+]    | MediEvil                          |        | Sealed    | CUSA 12982/RSC   | ![cover](./covers/PS4/CUSA_12982_RSC.jpg)
 [+]    | Mechwarrior 5: Mercenaries        |        | Sealed    | CUSA 28249       | ![cover](./covers/PS4/CUSA_28240.jpg)
 [+]    | Monster Hunter: World             |        | Sealed    | CUSA 07708       | ![cover](./covers/PS4/CUSA_07708.jpg)

### N
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | No Man's Sky                      |        |           | CUSA 03952/RSC   | ![cover](./covers/PS4/CUSA_03952_RSC.jpg)

### P
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Prey                              |        |           | CUSA 06559       | ![cover](./covers/PS4/CUSA_06559.jpg)

### R
 Status | The names of the games            | Region | Condition | Serial number    | Covers 
:------:|:----------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | RAGE 2                            |        | Sealed    | CUSA 10300       | ![cover](./covers/PS4/CUSA_10300.jpg)
 [+]    | Ratchet & Clank                   |        | Sealed    | CUSA 01073/H/RSC | ![cover](./covers/PS4/CUSA_01073_H_RSC.jpg)
 [+]    | Red Dead Redemtion                |        | Sealed    | CUSA 36843       | ![cover](./covers/PS4/CUSA_36843.jpg)
 [+]    | Red Dead Redemtion II             |        | Sealed    | CUSA 08519       | ![cover](./covers/PS4/CUSA_08519.jpg)
 [+]    | Resident Evil Origins Collection  |        | Sealed    | CUSA 02882       | ![cover](./covers/PS4/CUSA_02882.jpg)
 [+]    | Resident Evil 2: remake           |        | Sealed    | CUSA 09171       | ![cover](./covers/PS4/CUSA_09171.jpg)
 [+]    | Resident Evil 3: remake           |        | Sealed    | CUSA 14278       | ![cover](./covers/PS4/CUSA_14278.jpg)
 [+]    | Resident Evil 4                   |        | Sealed    | CUSA 04704       | ![cover](./covers/PS4/CUSA_04704.jpg)
 [+]    | Resident Evil 4: remake           |        | Sealed    | CUSA 33388       | ![cover](./covers/PS4/CUSA_33388.jpg)
 [+]    | Resident Evil 4: remake GE        |        | Sealed    | CUSA 33388       | ![cover](./covers/PS4/CUSA_33388_GE.jpg)
 [+]    | Resident Evil 5                   |        | Sealed    | CUSA 04284       | ![cover](./covers/PS4/CUSA_04284.jpg)
 [+]    | Resident Evil 6                   |        | Sealed    | CUSA 03840       | ![cover](./covers/PS4/CUSA_03840.jpg)
 [+]    | Resident Evil 7: Biohazard GE     |        | Sealed    | CUSA 09473       | ![cover](./covers/PS4/CUSA_09473.jpg)
 [+]    | Resident Evil: Village  GE        |        | Sealed    | CUSA 18008       | ![cover](./covers/PS4/CUSA_18008.jpg)
 [+]    | Resident Evil: Revelations        |        | Sealed    | CUSA 06212       | ![cover](./covers/PS4/CUSA_06212.jpg)
 [+]    | Resident Evil: Revelations 2      |        | Sealed    | CUSA 01133       | ![cover](./covers/PS4/CUSA_01133.jpg)
 [+]    | Redout Lightspeed Edition         |        |           | CUSA 01817       | ![cover](./covers/PS4/CUSA_01817.jpg)

### S
 Status | The names of the games                 | Region | Condition | Serial number    | Covers 
:------:|:---------------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Scott Pilligrim vs The World: The Game |        | Sealed    | 2108486          | ![cover](./covers/PS4/2108486.jpg)
 [+]    | Shadow of the Colossus                 |        | Sealed    | CUSA 08809       | ![cover](./covers/PS4/CUSA_08809.jpg)
 [+]    | Shadow of the Colossus                 |        |           | CUSA 08809/RSC   | ![cover](./covers/PS4/CUSA_08809_RSC.jpg)
 [+]    | Shantae                                |        |           | 2108839          | ![cover](./covers/PS4/2108839.jpg)
 [+]    | Signalis				 |        | Sealed    | CUSA 34423       | ![cover](./covers/PS4/CUSA_34423.jpg)
 [+]    | Signalis				 |        | Sealed    | PLJM 17101       | ![cover](./covers/PS4/PLJM_17101.jpg)
 [+]    | S.T.A.L.K.E.R: Legends Of The Zone     |        | Sealed    | PLJM 17362       | ![cover](./covers/PS4/PLJM_17362.jpg)
 [+]    | Stray				         |        | Sealed    | CUSA 24899       | ![cover](./covers/PS4/CUSA_24899.jpg)
 [+]    | Super Meat Boy		         |        | Sealed    | 2104994	         | ![cover](./covers/PS4/2104994.jpg)

### T
| Status | The names of the games                                   | Region | Condition |  Serial number   |                   Covers                    |
| :----: | :------------------------------------------------------- | :----: | :-------: | :--------------: | :-----------------------------------------: |
|  [+]   | The Order 1886                                           |        |  Sealed   |    CUSA 00076    | ![cover](./covers/PS4/CUSA_00076.jpg)    |
|  [+]   | The Last Guardian                                        |        |  Sealed   |    CUSA 03745    | ![cover](./covers/PS4/CUSA_03745.jpg)    |
|  [+]   | The Last of Us                                           |        |  Sealed   |   CUSA 00556/H   | ![cover](./covers/PS4/CUSA_00556_H.jpg)   |
|  [+]   | The Last of Us                                           |        |           | CUSA 00557/H/RSC | ![cover](./covers/PS4/CUSA_00557_H_RSC.jpg) |
|  [+]   | The Last of Us: Part II                                  |        |  Sealed   |    CUSA 10249    | ![cover](./covers/PS4/CUSA_10249.jpg)    |
|  [+]   | Tomb Raider: DE                                          |        |  Sealed   |    CUSA 00109    | ![cover](./covers/PS4/CUSA_00109.jpg)    |
|  [+]   | Tomb Raider: Rise of the Tomb Raider 20 Year Celebration |        |  Sealed   |    CUSA 05716    | ![cover](./covers/PS4/CUSA_05716.jpg)    |
|  [+]   | Tomb Raider: Shadow of the Tomb Raider                   |        |  Sealed   |    CUSA 10872    | ![cover](./covers/PS4/CUSA_10872.jpg)    |
|  [+]   | Tomb Raider: Lara Croft And the Temple of Osiris         |        |  Sealed   |    CUSA 00806    | ![cover](./covers/PS4/CUSA_00806.jpg)    |
|  [+]   | Tomb Raider: I II III Remaster                           |        |  Sealed   |    CUSA 43773    | ![cover](./covers/PS4/CUSA_43774.jpg)    |
|  [+]   | Ttitanfall 2                                             |        |           |    CUSA 04013    | ![cover](./covers/PS4/CUSA_04013.jpg)    |
|  [+]   | Turok                                                    |        |  Sealed   |     2108174      | ![cover](./covers/PS4/2108174.jpg)      |
|  [+]   | Turok 2: Seeds Of Evil                                   |        |  Sealed   |     2108173      | ![cover](./covers/PS4/2108173.jpg)      |
|  [+]   | Turok 3: Shadow Of Oblivion                              |        |  Sealed   |      211588      | ![cover](./covers/PS4/211588.jpg)      |

### U
 Status | The names of the games         | Region | Condition | Serial number    | Covers 
:------:|:-------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Uncharted 1-2-3 remastered     |        | Sealed    | CUSA 02343/H     | ![cover](./covers/PS4/CUSA_02343_H.jpg)
 [+]    | Uncharted 1-2-3 remastered     |        |           | CUSA 02344/H/RSC | ![cover](./covers/PS4/CUSA_02344_H_RSC.jpg)
 [+]    | Uncharted 4                    |        |           | CUSA 04529/H/RSC | ![cover](./covers/PS4/CUSA_04529_H_RSC.jpg)
 [+]    | Uncharted: Утраченное наследие |        |           | CUSA 09564/RSC   | ![cover](./covers/PS4/CUSA_09564_RSC.jpg)

### V
 Status | The names of the games         | Region | Condition | Serial number    | Covers 
:------:|:-------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | VOID Bastards                  |        | Sealed    | CUSA 16300       | ![cover](./covers/PS4/CUSA_16300.jpg)

### W
 Status | The names of the games                            | Region | Condition | Serial number    | Covers 
:------:|:--------------------------------------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Wipeout                                           |        | Sealed    | CUSA 05670       | ![cover](./covers/PS4/CUSA_05670.jpg)
 [+]    | Wolfenstein: The New Order + The Old Blood Double |        | Sealed    | CUSA 00320 01604 | ![cover](./covers/PS4/CUSA_00320_01604.jpg)
 [+]    | Wolfenstein: Youngblood DE                        |        | Sealed    | CUSA 13094       | ![cover](./covers/PS4/CUSA_13094.jpg)
 [+]    | Wolfenstein II: The New Colossus                  |        |           | CUSA 09357       | ![cover](./covers/PS4/CUSA_09357.jpg)

### Рус
 Status | The names of the games | Region | Condition | Serial number    | Covers 
:------:|:-----------------------|:------:|:---------:|:----------------:|:-------:
 [+]    | Грёзы                  |        | Sealed    | CUSA 04301/RSC   | ![cover](./covers/PS4/CUSA_04301_RSC.jpg)
 [+]    | Жизнь после            |        | Sealed    | CUSA 09176/RSC   | ![cover](./covers/PS4/CUSA_09176_RSC.jpg)
 [+]    | Человек-Паук           |        |           | CUSA 11995       | ![cover](./covers/PS4/CUSA_11995.jpg)
