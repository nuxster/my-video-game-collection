### A
 Status | The names of the games            | Region | Condition | Serial number | Covers 
:------:|:----------------------------------|:------:|:---------:|:-------------:|:-------:
 [+]    | Ace combat: Assault Horizon       |        | Sealed    | BLES 01393    | ![cover](./covers/PS3/BLES_01393.jpg)
 [+]    | Alien Isolation: Nostromo Edition |        |           | BLUS 30832D1  | ![cover](./covers/PS3/BLUS_30832D1.jpg)
 [+]    | Armored Core: For Answer          |        |           | BLES 00370    | ![cover](./covers/PS3/BLES_00370.jpg)
 [+]    | Armored Core 4                    |        |           | BLES 00039    | ![cover](./covers/PS3/BLES_00039.jpg)
 [+]    | Armored Core 5                    |        |           | BLES 01440    | ![cover](./covers/PS3/BLES_01440.jpg)
 [+]    | Armored Core: Verdict Day         |        | Sealed    | BLES 01898    | ![cover](./covers/PS3/BLES_01898.jpg)

### B
 Status | The names of the games      | Region | Condition | Serial number         | Covers 
:------:|:----------------------------|:------:|:---------:|:---------------------:|:-------:
 [+]    | Batman Arkham Collection    |        |           | BLES 01784            | ![cover](./covers/PS3/BLES_01784.jpg)
 [+]    | Borderlands Collection      |        | Sealed    | BLES 00697 BLES 01684 | ![cover](./covers/PS3/BLES_00697_BLES_01684.jpg)
 [+]    | Borderlands: The Pre-Sequel |        |           | BLES 02058            | ![cover](./covers/PS3/BLES_02058.jpg)

### C
 Status | The names of the games            | Region | Condition | Serial number         | Covers 
:------:|:----------------------------------|:------:|:---------:|:---------------------:|:-------:
 [ ]    | Castlevania: Lords of Shadow      |        |           |                       |
 [ ]    | Castlevania: Lords of Shadow 2    |        |           |                       |
 [ ]    | Call of Duty: World at War        |        |           |                       |
 [ ]    | Call of Duty 3                    |        |           |                       |
 [+]    | Call of Duty: MWF Trilogy         |        | Sealed    | 2101673               | ![cover](./covers/PS3/2101673.jpg)
 [+]    | Call of Duty: BlackOps Collection |        | Sealed    | 2102326               | ![cover](./covers/PS3/2102326.jpg)
 [+]    | Call of Duty: Ghosts              |        | Sealed    | BLES 01945            | ![cover](./covers/PS3/BLES_01945.jpg)
 [+]    | Crysis 2                          |        |           | BLES 01060/E          | ![cover](./covers/PS3/BLES_01060_E.jpg)
 [+]    | Crysis 3                          |        |           | BLES 01649            | ![cover](./covers/PS3/BLES_01649.jpg)

### D
 Status | The names of the games     | Region | Condition | Serial number         | Covers 
:------:|:---------------------------|:------:|:---------:|:---------------------:|:-------:
 [+]    | Dantes Inferno             |        | Sealed    | BLES 00713            | ![cover](./covers/PS3/BLES_00713.jpg)
 [ ]    | Deadpool                   |        |           |                       | 
 [+]    | Dead Space 1               |        |           | BLES 00308            | ![cover](./covers/PS3/BLES_00308.jpg)
 [+]    | Dead Space 2               |        |           | BLES 01189            | ![cover](./covers/PS3/BLES_01189.jpg)
 [+]    | Dead Space 2 LME           |        |           | BLES 01040            | ![cover](./covers/PS3/BLES_01040.jpg)
 [+]    | Dead Space 3 LME           |        |           | BLES 01733            | ![cover](./covers/PS3/BLES_01733.jpg)
 [+]    | Demon's Souls              |        |           | BLUS 30443GH          | ![cover](./covers/PS3/BLUS_30443GH.jpg)
 [+]    | Diablo III Reaper of Souls |        |           | BLES 02036            | ![cover](./covers/PS3/BLES_02036.jpg)
 [+]    | Doom 3 BFG Edition         |        |           | 2048891               | ![cover](./covers/PS3/2048891.jpg)
 [+]    | Duck Tales Remastered      |        | Sealed    | BLUS 31368            | ![cover](./covers/PS3/BLUS_31368.jpg)

### F
| Status | The names of the games | Region | Condition | Serial number |                 Covers                 
| :----: | :--------------------- | :----: | :-------: | :-----------: | :-------------------------------------:
|  [+]   | Fallout 3 GOTY         |        |           | BLES 00737/E  | ![cover](covers/PS3/BLES_00737_E.jpg)
|  [+]   | Fallout: New Vegas UE  |        |           | BLUS 30888GH  |
|  [ ]   | Fear 1                 |        |           |               |
|  [ ]   | Fear 2                 |        |           |               |
|  [ ]   | Fear 3                 |        |           |               |
|  [+]   | Front Mission: Evolved |        |           |  BLES 00788   | ![cover](./covers/PS3/BLES_00788.jpg)
|  [+]   | FarCry 2               |        |           | BLES 00324/E  | ![cover](./covers/PS3/BLES_00324_E.jpg)

### G
 Status | The names of the games              | Region | Condition | Serial number         | Covers 
:------:|:------------------------------------|:------:|:---------:|:---------------------:|:-------:
 [ ]    | Green Lantern                       |        |           |                       | 
 [+]    | GTA: SanAndreas                     |        |           | 2100981               | ![cover](./covers/PS3/2100981.jpg)
 [+]    | GTA IV & Episode Liberty City: CE   |        | Sealed    | BLES 01128            | ![cover](./covers/PS3/BLES_01128.jpg)
 [+]    | God of War (Бог войны) Collection 1 |        |           | BCUS 98229            | ![cover](./covers/PS3/BCUS_98229.jpg)
 [+]    | God of War (Бог войны) Collection 2 |        |           | BCUS 98289            | ![cover](./covers/PS3/BCUS_98289.jpg)
 [+]    | God of War 3                        |        |           | BCES 00510/E          | ![cover](./covers/PS3/BCES_00510_E.jpg)
 [+]    | God of War: Ascension (Стилбук)     |        |           | BCES 01741            | ![cover](./covers/PS3/BCES_01741.jpg)
 [ ]    | Godzilla                            |        |           |                       |
 [ ]    | Grand Turismo 5                     |        |           |                       |
 [ ]    | Grand Turismo 6                     |        |           |                       |

### H
 Status | The names of the games            | Region | Condition    | Serial number         | Covers 
:------:|:----------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Half-Life 2 The Orange Box (Силд) |        | Sealed       | BLES 00153            | ![covers](./covers/PS3/BLES_00153.jpg)
 [+]    | Heavenly Sword                    |        |              | BCES 00078/E          | ![covers](./covers/PS3/BCES_00078_E.jpg)

### I
 Status | The names of the games         | Region | Condition    | Serial number         | Covers 
:------:|:-------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Ico and Shadow of the Colossus |        |              | BSES 01097            | ![cover](./covers/PS3/BSES_01097.jpg)
 [+]    | Injustice UE                   |        |              | BLES 01967            | ![cover](./covers/PS3/BLES_01967.jpg)

### J
 Status | The names of the games      | Region | Condition    | Serial number         | Covers 
:------:|:----------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Jak and Daxter: The Trilogy |        | Sealed       | BCES 01325            | ![cover](./covers/PS3/BCES_01325.jpg)

### K
 Status | The names of the games | Region | Condition    | Serial number         | Covers 
:------:|:-----------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Killzone               |        |              | BCES 01743            | ![cover](./covers/PS3/BCES_01743.jpg)
 [+]    | Killzone 2             |        |              | BCES 00081            | ![cover](./covers/PS3/BCES_00081.jpg)
 [+]    | Killzone 3             |        |              | BCUS 90272            | ![cover](./covers/PS3/BCUS_90272.jpg)

### L
 Status | The names of the games         | Region | Condition    | Serial number         | Covers 
:------:|:-------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | LAIR                           |        |              | BSES 00004            | ![cover](./covers/PS3/BSES_00004.jpg)
 [ ]    | LittleBigPlanet                |        |              |                       |
 [ ]    | LittleBigPlanet 2              |        |              |                       |
 [+]    | LittleBigPlanet GOTY           |        |              | BCAS 20078            | ![cover](./covers/PS3/BCAS_20078.jpg)
 [+]    | LittleBigPlanet 2              |        |              | BCES 00850/S          | ![cover](./covers/PS3/BCES_00850_S.jpg)
 [+]    | LittleBigPlanet: Karting       |        |              | BCES 01422            | ![cover](./covers/PS3/BCES_01422.jpg)
 [+]    | Lollipop Chainsaw              |        | Sealed       | BLES 01525            | ![cover](./covers/PS3/BLES_01525.jpg)
 [+]    | Lost Planet: Extreme Condition |        | Sealed       | BLES 00198            | ![cover](./covers/PS3/BLES_00198.jpg)
 [+]    | Lost Planet 2                  |        |              | BLES 00710            | ![cover](./covers/PS3/BLES_00710.jpg)
 [+]    | Lost Planet 3                  |        | Sealed       | BLES 01685            | ![cover](./covers/PS3/BLES_01685.jpg)

### M
 Status | The names of the games        | Region | Condition    | Serial number         | Covers 
:------:|:------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Mass Effect: Trilogy          |        | Sealed       | BLUS 41000            | ![cover](./covers/PS3/BLUS_41000.jpg)
 [+]    | Metal Gear Solid: Trilogy HDC |        | Sealed       | BLUS 30847            | ![cover](./covers/PS3/BLUS_30847.jpg)
 [+]    | Mortal Kombat IX              |        |              | BLUS 30902GH          | ![cover](covers/PS3/BLUS_30902GH.jpg)
 [+]    | Mortal Kombat vs DC           |        |              | BLUS 41027            | ![cover](covers/PS3/BLUS_41027.jpg)
 [+]    | MotorStorm                    |        |              | BCES 00006            | ![cover](covers/PS3/BCES_00006.jpg)
 [+]    | MotorStorm: Pacific Rift      |        |              | BCES 00129            | ![cover](covers/PS3/BCES_00129.jpg)
 [+]    | MotorStorm: Apocalypse        |        |              | BCES 00484            | ![cover](covers/PS3/BCES_00484.jpg)
-
### N
 Status | The names of the games | Region | Condition    | Serial number         | Covers 
:------:|:-----------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Nail'd                 |        |              | BLES 00914            | ![cover](./covers/PS3/BLES_00914.jpg)

### O
 Status | The names of the games | Region | Condition    | Serial number         | Covers 
:------:|:-----------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Overlord: Raising Hell |        | Sealed       | BLES 00257            | ![cover](./covers/PS3/BLES_00257.jpg)
 [+]    | Overlord II            |        |              | BLES 00580            | ![cover](./covers/PS3/BLES_00580.jpg)

### P
 Status | The names of the games        | Region | Condition    | Serial number         | Covers 
:------:|:------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Portal 2                      |        | Sealed       | BLES 01222            | ![cover](./covers/PS3/BLES_01222.jpg)
 [+]    | Prince of Persia: Trilogy HDC |        | Sealed       | BLES 01092            | ![cover](./covers/PS3/BLES_01092.jpg)

### Q
 Status | The names of the games | Region | Condition    | Serial number         | Covers 
:------:|:-----------------------|:------:|:------------:|:---------------------:|:-------:
 [ ]    | Quake wars             |        |              |                       |

### R
 Status | The names of the games                | Region | Condition    | Serial number         | Covers 
:------:|:--------------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | RAGE                                  |        |              | BLES 01426            | ![cover](./covers/PS3/BLES_01426.jpg)
 [+]    | Ratchet & Clank: Tools of Destruction |        | Sealed       | BCES 00052/E          | ![cover](./covers/PS3/BCES_00052_E.jpg)
 [+]    | Ratchet & Clank: A Crack in Time      |        | Sealed       | BCES 00511/E          | ![cover](./covers/PS3/BCES_00511_E.jpg)
 [+]    | Ratchet & Clank: Q-Force              |        | Sealed       | BCES 01594            | ![cover](./covers/PS3/BCES_01594.jpg)
 [+]    | Resistance 1                          |        |              | BCUS 90663            | ![cover](./covers/PS3/BCUS_90663.jpg)
 [+]    | Resistance 2                          |        |              | BCUS 90664            | ![cover](./covers/PS3/BCUS_90664.jpg)
 [+]    | Resistance 3                          |        |              | BCES 01118            | ![cover](./covers/PS3/BCES_01118.jpg)
 [+]    | Resident Evil: Operation Raccoon City |        |              | BLES 01288            | ![cover](./covers/PS3/BLES_01288.jpg)
 [+]    | Remember ME                           |        |              | BLES 01701            | ![cover](./covers/PS3/BLES_01701.jpg)
 [+]    | Red Alert 3 UE                        |        | Sealed       | BLES 00506            | ![cover](./covers/PS3/BLES_00506.jpg)
 [+]    | RDR GOTY                              |        |              | BLUS 30758GH          |

### S
 Status | The names of the games              | Region | Condition    | Serial number         | Covers 
:------:|:------------------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Sacred: Гнев Малахима		      |        | Sealed	      | BLES 01492            | ![cover](./covers/PS3/BLES_01492.jpg)
 [+]    | Shaun White Snowboarding            |        |              | BLES 00445            | ![cover](./covers/PS3/BLES_00445.jpg)
 [+]    | Silent Hill HDC                     |        | Sealed       | BLUS 30810            | ![cover](./covers/PS3/BLUS_30810.jpg)
 [+]    | Sonic's Ultimate Genesis Collection |        |              | BLUS 30259GH          | ![cover](./covers/PS3/BLUS_30259GH.jpg)
 [+]    | Splinter Cell: Trilogy HDC          |        | Sealed       | BLES 01146            | ![cover](./covers/PS3/BLES_01146.jpg)

### T
 Status | The names of the games   | Region | Condition    | Serial number         | Covers 
:------:|:-------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Tomb Raider: Trilogy HDC |        |              | BLES 01195            | ![cover](./covers/PS3/BLES_01195.jpg)

### U
 Status | The names of the games         | Region | Condition    | Serial number         | Covers 
:------:|:-------------------------------|:------:|:------------:|:---------------------:|:-------:
 [ ]    | Uncharted: Drake’s Fortune     |        |              |                       | 
 [ ]    | Uncharted 2: Among Thieves     |        |              |                       |     
 [+]    | Uncharted 3: Drake’s Deception |        |              | BCES 01175/E          | ![cover](./covers/PS3/BCES_01175_E.jpg)
 [+]    | Unreal Tournament              |        |              | BLES 00200            | ![cover](./covers/PS3/BLES_00200.jpg)

### W
 Status | The names of the games | Region | Condition    | Serial number         | Covers 
:------:|:-----------------------|:------:|:------------:|:---------------------:|:-------:
 [ ]    | Watchmen               |        |              |                       |  
 [+]    | Wolfenshtein (2009)    |        |              | BLES 00564            | ![cover](./covers/PS3/BLES_00564.jpg)

### Z
 Status | The names of the games  | Region | Condition    | Serial number         | Covers 
:------:|:------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Zone Of The Enders: HDC |         |              | BLUS 30937L          | ![cover](./covers/PS3/BLUS_30937L.jpg)

### Рус
 Status | The names of the games  | Region | Condition    | Serial number         | Covers 
:------:|:------------------------|:------:|:------------:|:---------------------:|:-------:
 [+]    | Дурная репутация 1      |        |              | BCES 00609/E          | ![cover](./covers/PS3/BCES_00609_E.jpg)
 [+]    | Дурная репутация 2      |        |              | BCES 01143/E          | ![cover](./covers/PS3/BCES_01143_E.jpg)
 [+]    | Кукловод                |        | Sealed       | BCES 00935            | ![cover](./covers/PS3/BCES_00935.jpg)
 [+]    | Остаться в живых (LOST) |        |              | BLES 00221            | ![cover](./covers/PS3/BLES_00221.jpg)
 [+]    | Скрежет металла         |        |              | BCES 01010            | ![cover](./covers/PS3/BCES_01010.jpg)
