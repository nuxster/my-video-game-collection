### B
 Status | The names of the games | Region | Condition  | Serial number  | Covers 
:------:|:-----------------------|:------:|:----------:|:--------------:|:-------:
 [+]    | Burnout 3              | PAL    | Sealed     | SLES 52584     | ![cover](covers/PS2/SLES_52584.jpg)

### H
 Status | The names of the games | Region | Condition  | Serial number  | Covers 
:------:|:-----------------------|:------:|:----------:|:--------------:|:-------:
 [+]    | Half-Life              | PAL    |            | SLES 50504     | ![cover](covers/PS2/SLES_50504.jpg)

### N
 Status | The names of the games      | Region | Condition  | Serial number  | Covers 
:------:|:----------------------------|:------:|:----------:|:--------------:|:-------:
 [+]    | Need For Speed: Underground | PAL    | Sealed     | SLES 51967     | ![cover](covers/PS2/SLES_51967.jpg)

### R
 Status | The names of the games | Region | Condition  | Serial number  | Covers 
:------:|:-----------------------|:------:|:----------:|:--------------:|:-------:
 [+]    | Resident Evil 4        | PAL    | Sealed     | SLES 53702     | ![cpver](covers/PS2/SLES_53702.jpg)
