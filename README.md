# The repository contains a list of games from my video games collection and games that I would like to add to my collection.

[Web UI](https://my-video-game-collection-nuxster-768e3dac59de3bc9a4759b027f61e4.gitlab.io/)

**Xbox**
----
- [XBox Original ➤](docs/Games/XBox/XBox_Original_games.md)
- [XBox 360 ➤](docs/Games/XBox/XBox_360_games.md)
- [XBox One/Series ➤](docs/Games/XBox/XBox_One_Series_games.md)

**Nintendo**
----
- [Gameboy Advance ➤](Nintendo/Gameboy_Advance.md)
- [Switch ➤](docs/Games/Nintendo/Switch.md)

**Playstation**
----
- [Playstation 2 ➤](docs/Games/Playstation/PS2_games.md)
- [Playstation 3 ➤](docs/Games/Playstation/PS3_games.md)
- [Playstation 4 ➤](docs/Games/Playstation/PS4_games.md)
- [Playstation Vita ➤](docs/Games/Playstation/PSVita_games.md)

**PC**
----
- [Jawel Case ➤](docs/Games/PC/jawel.md)
- [DVD Box ➤](docs/Games/PC/dvdbox.md)
- [Big Box ➤](docs/Games/PC/bigbox.md)
- [Middle Box ➤](docs/Games/PC/middlebox.md)
